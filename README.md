# Solution of larger coupled sparse/dense linear systems in an industrial aeroacoustic context

[![pipeline status](https://gitlab.inria.fr/thesis-mfelsoci/slides/camus/badges/master/pipeline.svg)](https://gitlab.inria.fr/thesis-mfelsoci/slides/camus/-/commits/master)

[Slides for seminar in Camus team](https://thesis-mfelsoci.gitlabpages.inria.fr/slides/camus/camus.pdf)

